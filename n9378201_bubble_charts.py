
#-----Statement of Authorship----------------------------------------#
#
#  This is an individual assessment item.  By submitting this
#  code I agree that it represents my own work.  I am aware of
#  the University rule that a student must not act in a manner
#  which constitutes academic dishonesty as stated and explained
#  in QUT's Manual of Policies and Procedures, Section C/5.3
#  "Academic Integrity" and Section E/2.1 "Student Code of Conduct".
#
#    Student no: n9378201
#    Student name: Heath Mayocchi
#
#  NB: Files submitted without a completed copy of this statement
#  will not be marked.  All files submitted will be subjected to
#  software plagiarism analysis using the MoSS system
#  (http://theory.stanford.edu/~aiken/moss/).
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  BUBBLE CHARTS
#
#  This task tests your skills at defining functions, processing
#  data stored in lists and performing the arithmetic calculations
#  necessary to display a complex visual image.  The incomplete
#  Python script below is missing a crucial function,
#  "draw_bubble_chart".  You are required to complete this function
#  so that when the program is run it produces a bubble chart,
#  using data stored in a list to determine the positions and
#  sizes of the icons.  See the instruction sheet accompanying this
#  file for full details.
#
#--------------------------------------------------------------------#  



#-----Preamble and Test Data-----------------------------------------#
#
#

# Module provided
#
# You may use only the turtle graphics functions for this task;
# you may not import any other modules or files.

from turtle import *


# Given constants
#
# These constant values are used in the main program that sets up
# the drawing window; do not change any of these values.

max_value = 350 # maximum positive or negative value on the chart
margin = 25 # size of the margin around the chart
legend_width = 400 # space on either side of the window for the legend
window_height = (max_value + margin) * 2 # pixels
window_width = (max_value + margin) * 2 + legend_width # pixels
font_size = 12 # size of characters on the labels in the grid
grid_size = 50 # gradations for the x and y scales shown on the screen
tick_size = 5 # size of the ticks on either side of the axes, in pixels


# Test data
#
# These are the data sets that you will use to test your code.
# Each of the data sets is a list containing the specifications
# for several icons ("bubbles") to display on the screen.  Each
# such list element specifies one icon using four values:
#
#    [icon_style, x_value, y_value, z_value]
#
# The 'icon_style' is a character string specifying which icon to
# to display.  Possible icons are named 'Icon 0' to 'Icon 4'.
# The final three values are integers specifying the icon's values
# in three dimensions, x, y and z.  The x and y values will be in
# the range -350 to 350, and determine where to place the icon on
# the screen.  The z value is in the range 0 to 350, and determines
# how big the icon must be (i.e., its widest and/or highest size on
# the screen, in pixels).

# The first icon in three different sizes
data_set_00 = [['Icon 0', -200, 200, 20],
               ['Icon 0', 200, 200, 120],
               ['Icon 0', 0, 0, 100]]

# The second icon in four different sizes
data_set_01 = [['Icon 1', -200, 200, 20],
               ['Icon 1', 200, 200, 120],
               ['Icon 1', 200, -200, 60],
               ['Icon 1', 0, 0, 100]]

# The third icon in five different sizes
data_set_02 = [['Icon 2', -200, 200, 300],
               ['Icon 2', -200, -200, 30],
               ['Icon 2', 200, -200, 90],
               ['Icon 2', 200, 200, 120],
               ['Icon 2', 0, 0, 100]]

# The fourth icon in four different sizes
data_set_03 = [['Icon 3', -200, 200, 300],
               ['Icon 3', -200, -200, 30],
               ['Icon 3', 200, -200, 90],
               ['Icon 3', 0, 0, 100]]

# The fifth icon in four different sizes
data_set_04 = [['Icon 4', 200, 200, 190],
               ['Icon 4', -200, -200, 10],
               ['Icon 4', 200, -200, 90],
               ['Icon 4', 0, 0, 100]]

# The next group of data sets test all five of your icons
# at the same time

# All five icons at the same large size
data_set_05 = [['Icon 0', -200, 200, 200],
               ['Icon 1', 200, 200, 200],
               ['Icon 2', 200, -200, 200],
               ['Icon 3', -200, -200, 200],
               ['Icon 4', 0, 0, 200]]

# All five icons at another size, listed in a different order
data_set_06 = [['Icon 4', 0, 0, 150],
               ['Icon 3', -200, -200, 150],
               ['Icon 2', -200, 200, 150],
               ['Icon 1', 200, 200, 150],
               ['Icon 0', 200, -200, 150]]

# All five icons arranged diagonally, at increasing sizes
data_set_07 = [['Icon 0', -200, -200, 15],
               ['Icon 1', -100, -100, 50],
               ['Icon 2', 0, 0, 100],
               ['Icon 3', 100, 100, 120],
               ['Icon 4', 200, 200, 180]]

# An extreme test in which all five icons are VERY small
data_set_08 = [['Icon 0', -100, -80, 5],
               ['Icon 2', 100, -100, 1],
               ['Icon 3', 10, 30, 2],
               ['Icon 1', 100, 100, 0],
               ['Icon 4', 200, 200, 4]]

# The next group of data sets are intended as "realistic" ones
# in which all five icons appear once each at various sizes in
# different quadrants in the chart

# Data occurs in all four quadrants
data_set_09 = [['Icon 0', -265, -80, 50],
               ['Icon 2', 100, -146, 78],
               ['Icon 3', -50, 130, 69],
               ['Icon 1', 210, 100, 96],
               ['Icon 4', 200, 300, 45]]

# All data appears in the top quadrants
data_set_10 = [['Icon 4', -265, 80, 140],
               ['Icon 2', 100, 146, 24],
               ['Icon 1', 10, 30, 99],
               ['Icon 0', 210, 100, 75],
               ['Icon 3', 200, 300, 65]]

# All data appears in the top right quadrant
data_set_11 = [['Icon 3', 265, 80, 140],
               ['Icon 1', 100, 146, 24],
               ['Icon 2', 20, 30, 109],
               ['Icon 4', 210, 205, 75],
               ['Icon 0', 200, 300, 65]]

# All data appears in the bottom left quadrant
data_set_12 = [['Icon 2', -265, -110, 130],
               ['Icon 3', -100, -146, 34],
               ['Icon 0', -25, -40, 73],
               ['Icon 1', -210, -200, 75],
               ['Icon 4', -180, -320, 65]]

# Another case where data appears in all four quadrants
data_set_13 = [['Icon 4', -265, 80, 96],
               ['Icon 3', 100, -46, 54],
               ['Icon 2', 50, 30, 89],
               ['Icon 1', -210, -190, 75],
               ['Icon 0', 250, 300, 123]]

# Yet another set with data in all four quadrants
data_set_14 = [['Icon 1', 212, -165, 90],
               ['Icon 2', 153, -22, 125],
               ['Icon 3', 84, 208, 124],
               ['Icon 4', -105, -58, 85],
               ['Icon 0', -62, 274, 57]]

# A random test - it produces a different data set each time you run
# your program!
from random import randint
max_rand_coord = 300
min_size, max_size = 20, 200

data_set_15 = [[icon,
                randint(-max_rand_coord, max_rand_coord),
                randint(-max_rand_coord, max_rand_coord),
                randint(min_size, max_size)]
               for icon in ['Icon 0', 'Icon 1', 'Icon 2', 'Icon 3', 'Icon 4']]

# Finally, just for fun, a random test that produces a montage
# by plotting each icon twenty times (which obviously doesn't
# make sense as a real data set)
data_set_16 = [[icon,
                randint(-max_rand_coord, max_rand_coord),
                randint(-max_rand_coord, max_rand_coord),
                randint(min_size, max_size)]
               for icon in ['Icon 0', 'Icon 1', 'Icon 2', 'Icon 3', 'Icon 4'] * 20]

#***** If you want to create your own test data sets put them here

#
#--------------------------------------------------------------------#



#-----Student's Solution---------------------------------------------#
#
#  Define a function to call the icons to the screen using the
#  data sets
#--------------------------------------------------------------------#

def draw_bubble_chart(data_set):
    # Create a loop to process the cells from the data sets
    for cell in range(len(data_set)):
        # Declare variables for the cell positions
        xcoor = data_set[cell][1]
        ycoor = data_set[cell][2]
        zcoor = data_set[cell][3]
        # Define if statements for Icon number
        if data_set[cell][0] == 'Icon 0':
            Icon0(xcoor, ycoor, zcoor)
        elif data_set[cell][0] == 'Icon 1':
            Icon1(xcoor, ycoor, zcoor)
        elif data_set[cell][0] == 'Icon 2':
            Icon2(xcoor, ycoor, zcoor)
        elif data_set[cell][0] == 'Icon 3':
            Icon3(xcoor, ycoor, zcoor)
        elif data_set[cell][0] == 'Icon 4':
            Icon4(xcoor, ycoor, zcoor)


#-------------------------------------------------#   
# Define function for drawing Icon 0 (Android logo)
#
# I am using fractions to get the correct
# positions when scaling, I have had to rescale
# the zcor value to achieve (zcor = max height
# or width)
#-------------------------------------------------#

def Icon0(xcor, ycor, zcor):
    # Reduce zcor for icon sizing
    zcor = zcor*1.0/2
    # Go to starting position
    goto(xcor, ycor)
    # Reset width
    width(1)

    # Define function to set heading and move forward
    def move_to(heading, distance):
        setheading(heading)
        forward(distance)

    # Define function for legs
    def legs(zcor):
        pendown()
        begin_fill()
        # Draw left side
        move_to(270, zcor/4)
        # Draw to middle of bottom
        move_to(0, zcor/10)
        # Bottom dot
        dot(zcor/5)
        # Draw to end of bottom
        move_to(0, zcor/10)
        # Draw right side
        move_to(90, zcor/4)
        # Draw top
        move_to(180, zcor/5)
        end_fill()
        penup()

    # Define function for arms
    def arms(zcor):
        pendown()
        # Draw bottom dot
        dot(zcor/5)
        # Start to draw rectangle
        begin_fill()
        move_to(180, zcor/10)
        move_to(270, zcor*2/3)
        move_to(0, zcor/10)
        # Draw top dot
        dot(zcor/5)
        # Finish drawing rectangle
        forward(zcor/10)
        move_to(90, zcor*2/3)
        end_fill()
        penup()
        
    # Draw body
    penup()
    # Move to starting position
    move_to(270, zcor*7/10)
    pendown()
    begin_fill()
    color('#91B547')
    # Start body bottom
    move_to(0, zcor/2)
    # Draw body right side
    move_to(90, zcor)
    # Draw body top
    move_to(180, zcor)
    # Draw body left
    move_to(270, zcor)
    # Complete body bottom
    move_to(0, zcor/2)
    end_fill()
    penup()
    
    # Move to left leg start position
    goto(xcor, ycor)
    move_to(180, zcor*3/10)
    move_to(270, zcor*7/10)
    # Call function to draw legs
    legs(zcor)
    
    # Move to right leg start position
    move_to(0, zcor*4/10)
    # Call function to draw legs
    legs(zcor)
    
    # Move to right arm start position
    goto(xcor, ycor)
    move_to(0, zcor*7/10)
    move_to(90, zcor*1/5)
    # Call function to draw arms
    arms(zcor)
    
    # Move to left arm start position
    goto(xcor, ycor)
    move_to(180, zcor*7/10)
    move_to(90, zcor*1/5)
    # Call function to draw arms
    arms(zcor)
    
    # Move to head start position
    goto(xcor, ycor)
    move_to(90, zcor*4/10)
    move_to(180, zcor/2)
    # Draw start of head
    pendown()
    begin_fill()
    move_to(0, zcor)
    # Draw top curve of head
    # Set initial angle for the counter
    angle_counter = 105
    # Loop through number of steps required for the curve
    for steps in range(11):
        setheading(angle_counter)
        forward(zcor/7.5)
        # Increase the angle for each loop using the counter
        angle_counter = angle_counter + 15
    # Complete bottom of head
    move_to(0, zcor/2)
    end_fill()
    
    # Draw left antenna
    move_to(90, zcor*2/5)
    move_to(180, zcor*1/5)
    width(zcor*1/20)
    move_to(130, zcor*1/5)
    penup()
    backward(zcor*1/5)
    
    # Draw right antenna
    move_to(0, zcor*2/5)
    pendown()
    move_to(50, zcor*1/5)
    backward(zcor*1/5)
    
    # Draw right eye
    move_to(270, zcor*1/5)
    color('white')
    dot(zcor*1/8)
    penup()
    
    # Draw left eye
    move_to(180, zcor*2/5)
    pendown()
    dot(zcor*1/8)
    penup()


#-------------------------------------------------#
# Define function for drawing Icon 1 (CentOS Logo)
#-------------------------------------------------#

def Icon1(xcor, ycor, zcor):
    # Reduce zcor for icon sizing
    zcor = zcor*1.0/30
    # Reset width
    width(1)

    # Define a function to draw the arrows
    def arrows(colour, xcor_tri, ycor_tri, heading, xcor_rect, ycor_rect):
        penup()
        color(colour)
        
        # Go to arrow start position
        goto(xcor_tri, ycor_tri)
        # Set arrow direction
        setheading(heading)
        # Set shape and skew sides to suit logo
        shape('triangle')
        shapesize(stretch_wid=zcor*1/3, stretch_len=zcor*1/5)
        # Stamp image
        stamp()
        
        # Go to rectangle start position
        goto(xcor_rect, ycor_rect)
        # Set shape and side lengths to suit logo
        shape('square')
        shapesize(stretch_wid=zcor*1/42, stretch_len=zcor*1/1.8)
        # Stamp image
        stamp()
        penup()

    # Draw N orange arrow
    arrows('#FFB870', xcor, ycor+zcor*13, 90, xcor, ycor+zcor*7.1)
    # Draw NE light purple arrow
    arrows('#A300A3', xcor+zcor*9, ycor+zcor*9, 45, xcor+zcor*5.1,ycor+zcor*5.1)
    # Draw E dark purple arrow
    arrows('#330080', xcor+zcor*13, ycor, 0, xcor+zcor*7.1, ycor)
    # Draw SE orange arrow
    arrows('#FFB870', xcor+zcor*9, ycor-zcor*9, 315, xcor+zcor*5.1, ycor-zcor*5.1)
    # Draw S green arrow
    arrows('#9CE653', xcor, ycor-zcor*13, 270, xcor, ycor-zcor*7.1)
    # Draw SW dark purple arrow
    arrows('#330080', xcor-zcor*9, ycor-zcor*9, 225, xcor-zcor*5.1,ycor-zcor*5.1)
    # Draw W light purple arrow
    arrows('#A300A3', xcor-zcor*13, ycor, 180, xcor-zcor*7.1,ycor)
    # Draw NW green arrow
    arrows('#9CE653', xcor-zcor*9, ycor+zcor*9, 135, xcor-zcor*5.1,ycor+zcor*5.1)
	
    # Define a function to draw the centre rhomboids   
    def rhomboids(int_xcor, int_ycor, colour, heading1, heading2, heading3, heading4):
        # Go to start position
        goto(int_xcor, int_ycor)
        color(colour)
        begin_fill()
        # Set heading and draw sides
        setheading(heading1)
        forward(zcor*7.5)
        setheading(heading2)
        forward(zcor*3)
        setheading(heading3)
        forward(zcor*3.2)
        setheading(heading4)
        forward(zcor*7.5)
        end_fill()
        penup()

    # Draw NNE dark purple rhomboids
    rhomboids(xcor+zcor*1.2, ycor+zcor*3.2, '#A300A3', 45, 135, 180, 270)
    # Draw ENE dark purple rhomboids
    rhomboids(xcor+zcor*3.2, ycor+zcor*1.2, '#A300A3', 0, 90, 135, 225)
    # Draw ESE orange rhomboids
    rhomboids(xcor+zcor*3.2, ycor-zcor*1.2, '#FFB870', 315, 45, 90, 180)
    # Draw SSE orange rhomboids
    rhomboids(xcor+zcor*1.2, ycor-zcor*3.2, '#FFB870', 270, 0, 45, 135)
    # Draw SSW dark purple rhomboids
    rhomboids(xcor-zcor*1.2, ycor-zcor*3.2, '#330080', 225, 315, 0, 90)
    # Draw WSW dark purple rhomboids
    rhomboids(xcor-zcor*3.2, ycor-zcor*1.2, '#330080', 180, 270, 315, 45)
    # Draw WNW green rhomboids
    rhomboids(xcor-zcor*3.2, ycor+zcor*1.2, '#9CE653', 135, 225, 270, 0)
    # Draw WNW green rhomboids
    rhomboids(xcor-zcor*1.2, ycor+zcor*3.2, '#9CE653', 90, 180, 225, 315)

	
#-------------------------------------------------#
# Define function for drawing Icon 2 (iOS Logo)
#
# I didn't have knowledge of the
# circle(radius, extent = n, steps = n) function
# when I made this, so I used a loop to step
# through angles and make a curve
#-------------------------------------------------#

def Icon2(xcor, ycor, zcor):
    # Reduce zcor for icon sizing
    zcor = (zcor*0.55)
    # Reset width
    width(1)
    penup()
    # Go to start position
    goto(xcor, ycor)
    setheading(90)
    forward(zcor/2)
    setheading(180)
    forward(zcor*1/10)
    begin_fill()
    # Set color
    fillcolor('white')
    pencolor('white')

    # Define function to draw the curves
    def curve(int_angle, steps, forward_size, angle_counter):
        # Set initial angle for the counter
        angle = (int_angle)
        # Loop through number of steps required for the curve
        for step in range(steps):   
            setheading(angle) 
            forward(forward_size)
            # Increase the angle for each loop using the counter
            angle = angle + angle_counter
            
    # Draw apple using the curve function
    pendown()
    # Starting at the centre of the top side
    # moving right and drawing the whole apple
    # in one drawing
    curve(0, 6, (zcor*0.025), 3)
    curve(18, 6, (zcor*0.125), -15)
    curve(225, 8, (zcor*0.1), 15)
    setheading(240)
    forward(zcor/3)
    curve(240, 6, (zcor*1/12), -15)
    curve(135, 10, (zcor*1/30), 10)
    curve(200, 5, (zcor*1/12), -15)
    curve(125, 7, (zcor*0.125), -5)
    curve(90, 3, (zcor*1/12), -10)
    curve(60, 3, (zcor*1/12), -10)
    curve(30, 3, (zcor*1/12), -15)
    goto(xcor, ycor+(zcor/2))    
    end_fill()
    penup()
    
    # Go to start position for leaf
    goto(xcor, ycor)
    setheading(90)
    forward(zcor*0.55)
    setheading(180)
    forward(zcor*0.1)
    # Draw leaf using the curve function
    pendown()
    begin_fill()
    curve(15, 6, (zcor*1/12), 15)
    curve(195, 6, (zcor*1/12), 15)
    end_fill()
    penup()


#------------------------------------------------#
# Define function for drawing Icon 3 (Mageia Logo)
#------------------------------------------------#

def Icon3(xcor, ycor, zcor):
    # Reduce zcor for icon sizing
    zcor = zcor * 1.0/1.1
    # Reset width
    width(1)
    penup()
    color('#00002E')
    # Set pen width for pot
    width(zcor * 1/9)
    
    # Go to the start position of the pot
    goto(xcor, ycor)
    setheading(90)
    forward(zcor * 0.1)
    setheading(0)
    forward(zcor * 1/4.5)
    # Set initial direction and draw pot
    pendown()
    setheading(140)
    circle(zcor * 1/3, extent = -275, steps = 30)
    setheading(300)
    circle(zcor * 0.25, extent = 120, steps = 6)
    penup()

    # Define a function to draw the bubbles
    def bubble(xcor, ycor, dot_size):
        color('#0066FF')
        # Go to start position
        goto(xcor, ycor)
        # Draw dot
        dot(dot_size)
    
    # Move and draw first dot (bottom)
    bubble(xcor - (zcor * 0.125), ycor + (zcor * 1/5), zcor * 0.125)
    # Move and draw second dot
    bubble(xcor + (zcor * 0.1), ycor + (zcor * 0.25), zcor * 1/9)
    # Move and draw third dot
    bubble(xcor - (zcor * 1/36), ycor + (zcor * 1/2.8), zcor * 0.1)
    # Move and draw fourth dot
    bubble(xcor + (zcor * 0.0625), ycor + (zcor * 1/2.2), zcor * 1/11)
    # Move and draw fifth dot (top)
    bubble(xcor - (zcor * 0.05), ycor + (zcor * 1/1.9), zcor * 1/12)
    penup()

	
#-------------------------------------------------#
# Define function for drawing Icon 4 (Windows logo)
#
# I didn't have knowledge of the
# circle(radius, extent = n, steps = n) function
# when I made this, so I used a loop to step
# through angles and make a curve
#-------------------------------------------------#

def Icon4(xcor, ycor, zcor):
    
    # Define function to draw rhomboids    
    def rhomboids(xcor, ycor, colour, int_top_angle, int_bottom_angle, steps):
        penup()
        # Go to centre position
        goto(xcor, ycor)
        begin_fill()
        color(colour)
        # Reset width
        width(1)
        pendown()
        # Draw right side
        setheading(60)
        forward(zcor/3.2)
        # Set counter to initial angle and draw top curve
        angle = int_top_angle
        for step in range(steps):    
            setheading(angle) 
            forward(zcor/16)
            # Use counter to increase turn by 15 degrees every step
            angle = angle + 15
        # Draw left side
        setheading(240)
        forward(zcor/3.2)
        # Set counter to initial angle and draw bottom curve
        angle = int_bottom_angle
        for step in range(steps):    
            setheading(angle) 
            forward(zcor/16)
            # Use counter to decrease turn by 15 degrees every step
            angle = angle - 15
        end_fill()
        penup()
    
    # Call functions to draw rhomboids of Windows logo
    rhomboids(xcor-(zcor/40), ycor+(zcor/40), 'red1', 140, 35, 6)
    rhomboids(xcor-(zcor/5), ycor-(zcor/3.6), 'deepskyblue2', 140, 35, 6)
    rhomboids(xcor+(zcor/40), ycor+(zcor/40), 'green3', 315, 210, 6)
    rhomboids(xcor-(zcor/6.5), ycor-(zcor/3.6), 'gold1', 315, 210, 6)
	
	
#----------------------------------------------#
# Define function for drawing the graph legend
#----------------------------------------------#

def legend():
    # Go to inner starting position
    goto(max_value + margin, max_value - margin * 1.75)
    # Reset width
    width(1)
    
    # Draw legend frame
    pendown()    
    # Draw inner top line
    setheading(0)
    forward(legend_width/2 - margin)
    # Draw inner right line
    setheading(270)
    forward(legend_width + margin * 6)
    # Draw inner bottom line
    setheading(180)
    forward(legend_width/2 - margin)
    # Draw inner left line
    setheading(90)
    forward(legend_width + margin * 6)
    penup()

    # Go to outer starting position
    setheading(135)
    forward(5)
    # Draw outer top line
    pendown()
    setheading(0)
    forward(legend_width/2 - margin + 7.5)
    # Draw outer right line
    setheading(270)
    forward(legend_width + (margin * 6) + 7.75)
    # Draw outer bottom line
    setheading(180)
    forward(legend_width/2 - margin + 7.5)
    # Draw outer left line
    setheading(90)
    forward(legend_width + (margin * 6) + 7.75)
    penup()
    
    # Write legend header, centred and bold
    goto(max_value + margin * 4.5, max_value - (margin * 3))
    write('Operating Systems', align = "center", font=("Arial", font_size, "bold"))
    
    # Define a function to write the icon labels, right justified
    def label(logo_name, label_y_position):
        goto(margin * 22 - legend_width / 40, label_y_position)
        write (logo_name, align = "right", font=("Arial", font_size, "normal"))

    # Write icon labels to the screen
    label('Android', margin * 7.5)
    label('CentOS', margin * 3.5)
    label('iOS', - margin / 2)
    label('Mageia', - margin * 4.5)
    label('Windows', - margin * 8.5)
        
    # Call the Icon functions to draw the legend icons
    Icon0(legend_width + margin, margin * 8, 70)
    Icon1(legend_width + margin, margin * 4, 70)
    Icon2(legend_width + margin, margin - margin, 70)
    Icon3(legend_width + margin, - margin * 4, 70)
    Icon4(legend_width + margin, - margin * 8, 70)

    # Write signature
    color('black')
    signature_font_size = 10
    goto(max_value + margin * 4.5, - margin * 11)
    write ('Author: Heath Mayocchi', align = "center", font=("Arial", signature_font_size, "italic"))
    goto(max_value + margin * 4.5, - margin * 11.75)
    write ('Student #: n9378201', align = "center", font=("Arial", signature_font_size, "italic"))  

#
#--------------------------------------------------------------------#



#-----Main Program---------------------------------------------------#
#
# This main program sets up the drawing environment, ready for you
# to start drawing your bubble chart.  Do not change any of
# this code except the lines marked '*****'
    
# Set up the drawing window with enough space for the grid and
# legend
setup(window_width, window_height)
title('Operating Systems Bubble Chart') #***** Choose a title appropriate to your icons

# Draw as quickly as possible by minimising animation
hideturtle()     #***** You may comment out this line while debugging
                 #***** your code, so that you can see the turtle move
speed('fastest') #***** You may want to slow the drawing speed
                 #***** while debugging your code

# Choose a neutral background colour                    
bgcolor('grey')

# Draw the two axes
pendown() # assume we're at home, facing east
forward(max_value)
left(180) # face west
forward(max_value * 2)
home()
setheading(90) # face north
forward(max_value)
left(180) # face south
forward(max_value * 2)
penup()



# Draw each of the tick marks and labels on the x axis
for x_coord in range(-max_value, max_value + 1, grid_size):
    if x_coord != 0: # don't label zero
        goto(x_coord, -tick_size)
        pendown()
        goto(x_coord, tick_size)
        penup()
        write(str(x_coord), align = 'center',
              font=('Arial', font_size, 'normal'))
        
# Draw each of the tick marks and labels on the y axis
for y_coord in range(-max_value, max_value + 1, grid_size):
    if y_coord != 0: # don't label zero
        goto(-tick_size, y_coord)
        pendown()
        goto(tick_size, y_coord)
        penup()
        goto(tick_size, y_coord - font_size / 2) # Allow for character height
        write('  ' + str(y_coord), align = 'left',
              font=('Arial', font_size, 'normal'))

# Draw legend
legend()

## Function call to draw a test logo
# Icon3(0, 0, 100)

# Call the student's function to display the data set
draw_bubble_chart(data_set_14) #***** Change this for different data sets
    
# Exit gracefully
hideturtle()
done()

#
#--------------------------------------------------------------------#




